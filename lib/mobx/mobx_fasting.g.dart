// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'mobx_fasting.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$MobxFasting on MobxFastingBase, Store {
  final _$timerAtom = Atom(name: 'MobxFastingBase.timer');

  @override
  int get timer {
    _$timerAtom.reportRead();
    return super.timer;
  }

  @override
  set timer(int value) {
    _$timerAtom.reportWrite(value, super.timer, () {
      super.timer = value;
    });
  }

  final _$idFeelingOptionAtom = Atom(name: 'MobxFastingBase.idFeelingOption');

  @override
  int get idFeelingOption {
    _$idFeelingOptionAtom.reportRead();
    return super.idFeelingOption;
  }

  @override
  set idFeelingOption(int value) {
    _$idFeelingOptionAtom.reportWrite(value, super.idFeelingOption, () {
      super.idFeelingOption = value;
    });
  }

  final _$MobxFastingBaseActionController =
      ActionController(name: 'MobxFastingBase');

  @override
  void setTimer(int val) {
    final _$actionInfo = _$MobxFastingBaseActionController.startAction(
        name: 'MobxFastingBase.setTimer');
    try {
      return super.setTimer(val);
    } finally {
      _$MobxFastingBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setIdFeeling(int val) {
    final _$actionInfo = _$MobxFastingBaseActionController.startAction(
        name: 'MobxFastingBase.setIdFeeling');
    try {
      return super.setIdFeeling(val);
    } finally {
      _$MobxFastingBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
timer: ${timer},
idFeelingOption: ${idFeelingOption}
    ''';
  }
}
