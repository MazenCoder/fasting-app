import 'package:mobx/mobx.dart';
import 'dart:io';

part 'mobx_fasting.g.dart';

class MobxFasting = MobxFastingBase with _$MobxFasting;

abstract class MobxFastingBase with Store {



  @observable
  int timer = 0;

  @action
  void setTimer(int val) {
    this.timer = val;
  }

  @observable
  int idFeelingOption;

  @action
  void setIdFeeling(int val) {
    this.idFeelingOption = val;
  }
}