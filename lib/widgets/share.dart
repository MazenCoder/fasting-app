import 'package:fasting_app/screens/tabs_view/fasting/fasting.dart';
import 'package:sleek_circular_slider/sleek_circular_slider.dart';
import 'package:fasting_app/constants/colors.dart';
import 'package:fasting_app/constants/routes.dart';
import 'package:timer_builder/timer_builder.dart';
import 'package:fasting_app/util/app_utils.dart';
import 'package:fasting_app/models/models.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:share/share.dart';
import 'package:time/time.dart';



class ShareWidget extends StatelessWidget {
  ShareWidget({
    Key key,
    @required this.size,
    @required this.initCircle,
    @required this.context,
    @required this.details,
  }) : super(key: key);

  final Size size;
  final int initCircle;
  final BuildContext context;
  final UserDetails details;
  final AppUtils appUtils = GetIt.I.get<AppUtils>();


  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          )),
      height: size.height / 1.2,
      padding: EdgeInsets.all(15),
      child: Column(
        children: [
          GestureDetector(
            onTap: () => AppRoutes.pop(context),
            child: Align(
              alignment: Alignment.topRight,
              child: Container(
                margin: EdgeInsets.only(bottom: 20),
                height: 20,
                width: 20,
                //alignment: Alignment.topRight,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(width: 2, color: Colors.grey)),
                child: Icon(
                  Icons.clear,
                  size: 15,
                ),
              ),
            ),
          ),
          SleekCircularSlider(
            initialValue: initCircle+ 0.0,
            innerWidget: (val) {
              return Center(
                child: SizedBox(
                  child: CircleAvatar(
                    radius: size.width / 3,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text(
                          'Time since Last Fast\n',
                          style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold,
                          ),
                        ),
                        TimerBuilder.periodic(
                            Duration(seconds: 1),
                            alignment: Duration.zero,
                            builder: (context) {
                              var now = DateTime.now();
                              var remaining = now.difference(start);
                              print('remaining: $remaining');
                              final Duration oneHourThirtyMinutes = hour.hours - remaining;
                              return Text("${appUtils.formatDuration(oneHourThirtyMinutes)}",
                                style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.bold,
                                ),
                              );
                            }
                        ),
                        SizedBox(height: 8),
                        Text(
                          'Up coming fast\n',
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        ),
                        Text(
                          '$hour hours\n',
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            },
            appearance: CircularSliderAppearance(
                customWidths: CustomSliderWidths(
                  trackWidth: 5,
                  handlerSize: 20,
                  progressBarWidth: 12,
                ),
                size: size.width / 1.2,
                customColors: CustomSliderColors(
                  gradientStartAngle: 0,
                  dotColor: Color(0xff3E28B6),
                  gradientEndAngle: 270,
                  progressBarColors: AppColors.barGradient.colors,
                ),
            ),
            onChange: (val) {
              print('$val');
            },
          ),
          Container(
            width: 150,
            margin: EdgeInsets.only(top: 15, bottom: 20),
            alignment: Alignment.center,
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                border: Border.all(color: AppColors.green, width: 2)),
            child: Text(
              '${details.fastType}',
              style: TextStyle(
                color: AppColors.green,
              ),
            ),
          ),
          GestureDetector(
            onTap: () => Share.share('Share me'),
            child: Align(
              alignment: Alignment.centerRight,
              child: Container(
                width: 30,
                height: 30,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: AppColors.darkPink),
                child: Icon(
                  Icons.share,
                  color: Colors.white,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
