import 'package:flutter_sparkline/flutter_sparkline.dart';
import 'package:fasting_app/util/firebase_service.dart';
import 'package:fasting_app/mobx/mobx_fasting.dart';
import 'package:fasting_app/constants/colors.dart';
import 'package:fasting_app/constants/routes.dart';
import 'package:fasting_app/util/app_utils.dart';
import 'package:fasting_app/models/journal.dart';
import 'package:fasting_app/models/models.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:uuid/uuid.dart';
import '../constants.dart';



class JournalAdd extends StatelessWidget {
  final bool pop;
  final bool button;
  final UserDetails userDetails;

  JournalAdd({
    Key key,
    @required this.pop,
    @required this.button,
    @required this.userDetails,
  }) : super(key: key);

  final FirebaseService firebaseUtil = GetIt.I.get<FirebaseService>();
  final AppUtils appUtils = GetIt.I.get<AppUtils>();
  final MobxFasting _mobx = MobxFasting();
  final _form = GlobalKey<FormState>();

  final TextEditingController controllerNote = TextEditingController();
  final TextEditingController controllerWeight = TextEditingController();
  var data = [0.0, 1.0, 1.5, 2.0, 0.0, 0.0, -0.5, -1.0, -0.5, 0.0, 0.0];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SingleChildScrollView(
        child: Container(
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(40),
                topRight: Radius.circular(40),
              ),
          ),
          // height: size.height / 1.2,
          padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
          child: Padding(
            padding: EdgeInsets.fromLTRB(15, 30, 15, 0.0),
            child: Form(
              key: _form,
              child: Column(
                children: [
                  pop ? GestureDetector(
                    onTap: () => AppRoutes.pop(context),
                    child: Align(
                      alignment: Alignment.topRight,
                      child: Container(
                        margin: EdgeInsets.only(bottom: 20),
                        height: 20,
                        width: 20,
                        //alignment: Alignment.topRight,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(width: 2, color: Colors.grey)),
                        child: Icon(
                          Icons.clear,
                          size: 15,
                        ),
                      ),
                    ),
                  )
                      : Container(),
                  Text(
                    'Update your Journal',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                    ),
                  ),
                  Card(
                    margin: EdgeInsets.only(top: 15, bottom: 15),
                    color: AppColors.greyBack,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)),
                    child: Container(
                      padding: EdgeInsets.all(8),
                      child: Column(
                        children: [
                          Text(
                            'How are you feeling? ',
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 18),
                          ),
                          Wrap(
                            alignment: WrapAlignment.center,
                            children: feelingOptions.entries.map((e) {
                              return Observer(
                                builder: (_) {
                                  return Material(
                                    borderRadius: BorderRadius.circular(100),
                                    color: e.key == _mobx.idFeelingOption ? AppColors.secondaryColor : null,
                                    child: Padding(
                                      padding: const EdgeInsets.all(1),
                                      child: IconButton(
                                        icon: ImageIcon(
                                          AssetImage(
                                            e.value,
                                          ),
                                          color: e.key == _mobx.idFeelingOption ?
                                          Colors.white : AppColors.secondaryColor,
                                        ),
                                        onPressed: () {
                                          print(e.key);
                                          _mobx.setIdFeeling(e.key);
                                        },
                                      ),
                                    ),
                                  );
                                },
                              );
                            }).toList(),
                          )
                        ],
                      ),
                    ),
                  ),

                  Card(
                    margin: EdgeInsets.only(bottom: 15),
                    color: AppColors.greyBack,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Container(
                      padding: EdgeInsets.all(10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('Mood Graph',
                            style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold,
                            ),
                          ),

                          Stack(
                            children: <Widget>[
                              AspectRatio(
                                aspectRatio: 1.15,
                                child: Container(
                                  decoration: const BoxDecoration(
                                    // color: Colors.amber,
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(18),
                                    ),
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                      right: 40.0, left: 12.0, top: 24, bottom: 12,
                                    ),
                                    child: FutureBuilder<List<double>>(
                                      future: appUtils.getSparklineData(),
                                      builder: (context, snapshot) {
                                        switch(snapshot.connectionState) {
                                          case ConnectionState.waiting: return Center(
                                            child: CircularProgressIndicator(),
                                          );
                                          default:

                                            if (snapshot.data == null ||
                                                snapshot.data.isEmpty ||
                                                snapshot.data.length == 1) {
                                              return Container();
                                            }

                                            return Sparkline(
                                              data: snapshot.data,
                                              lineColor: Colors.purple,
                                              pointsMode: PointsMode.all,
                                              pointSize: 8.0,
                                            );
                                        }
                                      },
                                    ),
                                  ),
                                ),
                              ),

                              Positioned(
                                right: 0,
                                child: Container(
                                  height: 300,
                                  child: Column(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: feelingOptions.entries.map((e) {
                                      return ImageIcon(
                                        AssetImage(
                                          e.value,
                                        ),
                                      );
                                    }).toList(),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  Card(
                    margin: EdgeInsets.only(bottom: 15),
                    color: AppColors.greyBack,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Container(
                      padding: EdgeInsets.all(10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('Add a Note',
                            style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold,
                            ),
                          ),

                          Container(
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: TextField(
                              maxLines: 5,
                              controller: controllerNote,
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.symmetric(horizontal: 7),
                                border: InputBorder.none,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Card(
                    margin: EdgeInsets.only(bottom: 15),
                    color: AppColors.greyBack,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Container(
                      padding: EdgeInsets.all(10),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('Add Weight',
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.bold),
                            ),
                            Container(
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10),
                              ),
                              child: TextFormField(
                                //maxLines: 5,
                                controller: controllerWeight,
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                    contentPadding:
                                    EdgeInsets.symmetric(horizontal: 7),
                                    border: InputBorder.none,
                                ),
                                validator: (val) {
                                  if(val.isEmpty) {
                                    return 'Required field';
                                  } else return null;
                                },
                              ),
                            )
                          ]),
                    ),
                  ),
                  button ? Container(
                    margin: EdgeInsets.only(bottom: 20),
                    width: 200,
                    child: RaisedButton(
                      onPressed: () async {
                        if (_form.currentState.validate()) {
                          FocusScope.of(context).unfocus();
                          final id = Uuid().v4();
                          final journal = Journal(
                            id: id,
                            uid: firebaseUtil.user.uid,
                            staredFasting: userDetails.fastStartedAt,
                            goalFasting: DateTime.now().toString(),
                            idFeeling: _mobx.idFeelingOption,
                            weight: controllerWeight.text.trim(),
                            note: controllerNote.text.trim(),
                          );
                          await appUtils.addJournal(journal, id);
                          Navigator.pop(context);
                        }
                      },
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                      color: AppColors.green,
                      child: Text(
                        'Save',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  )
                      : Container()
                ],
              ),
            )
          ),
        ),
      ),
    );
  }
}
