import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';
import 'package:fasting_app/screens/tabs_view/fasting/fasting.dart';
import 'package:fasting_app/constants/colors.dart';
import 'package:timer_builder/timer_builder.dart';
import 'package:fasting_app/util/app_utils.dart';
import 'package:get_it/get_it.dart';
import 'package:flutter/material.dart';
import 'package:jiffy/jiffy.dart';
import 'package:time/time.dart';



class TargetWidget extends StatelessWidget {
  TargetWidget({Key key}) : super(key: key);

  final AppUtils appUtils = GetIt.I.get<AppUtils>();


  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 20, right: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Flexible(
              flex: 3,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Start',
                    style: TextStyle(
                        color: AppColors.secondaryColor,
                        fontSize: 18,
                        fontWeight: FontWeight.bold),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: AutoSizeText(
                      Jiffy(start).format("MMMM do yyyy"),
                      maxLines: 1,
                      style: TextStyle(
                        color: AppColors.darkPink,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Text(
                    Jiffy(start).format("h:mm:ss a"),
                    style: TextStyle(
                      color: AppColors.darkPink,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              )),
          Flexible(
            flex: 2,
            child: fastStarted ?

              TimerBuilder.periodic(
                  Duration(seconds: 1),
                  alignment: Duration.zero,
                  builder: (context) {
                    var now = DateTime.now();
                    var remaining = now.difference(start);
                    print('remaining: $remaining');
                    final Duration oneHourThirtyMinutes = hour.hours - remaining;
                    return Text("${appUtils.formatDuration(oneHourThirtyMinutes)}");
                  }
              )
                : Text(
                    "00:00:00",
                    style: TextStyle(
                      color: AppColors.darkPink,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
          ),
          Flexible(
              flex: 3,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text(
                    'End',
                    style: TextStyle(
                      color: AppColors.secondaryColor,
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: AutoSizeText(
                      Jiffy(end).format("MMMM do yyyy",),
                      textAlign: TextAlign.end,
                      maxLines: 1,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: AppColors.darkPink,
                      ),
                    ),
                  ),
                  Text(
                    Jiffy(end).format("h:mm:ss a"),
                    textAlign: TextAlign.end,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: AppColors.darkPink,
                    ),
                  ),
                ],
              )),
        ],
      ),
    );
  }
}
