import 'package:flutter_sparkline/flutter_sparkline.dart';
import 'package:fasting_app/util/app_utils.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import '../../constants.dart';



class LineChartSample2 extends StatefulWidget {
  @override
  _LineChartSample2State createState() => _LineChartSample2State();
}

class _LineChartSample2State extends State<LineChartSample2> {

  final AppUtils appUtils = GetIt.I.get<AppUtils>();

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        AspectRatio(
          aspectRatio: 1.15,
          child: Container(
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.all(
                Radius.circular(18),
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.only(
                right: 40.0, left: 12.0, top: 24, bottom: 12,
              ),
              child:FutureBuilder<List<double>>(
                future: appUtils.getSparklineData(),
                builder: (context, snapshot) {
                  switch(snapshot.connectionState) {
                    case ConnectionState.waiting: return Center(
                      child: CircularProgressIndicator(),
                    );
                    default:
                      if (snapshot.data == null ||
                          snapshot.data.isEmpty ||
                          snapshot.data.length == 1) {
                        return Container();
                      }

                      return Sparkline(
                        data: snapshot.data,
                        lineColor: Colors.purple,
                        pointsMode: PointsMode.all,
                        pointSize: 8.0,
                      );
                  }
                },
              ),

              // child: LineChart(
              //   mainData(),
              // ),
            ),
          ),
        ),

        Positioned(
          right: 0,
          child: Container(
            height: 300,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: feelingOptions.entries.map((e) {
                return ImageIcon(
                  AssetImage(
                    e.value,
                  ),
                );
              }).toList(),
            ),
          ),
        ),
      ],
    );
  }
}
