import 'package:fasting_app/screens/tabs_view/profile/settings/settings.dart';
import 'package:fasting_app/widgets/charts/total_fasting_chart.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart' as cloud;
import 'package:fasting_app/widgets/charts/fasting_chart.dart';
import 'package:fasting_app/widgets/charts/weight_chart.dart';
import 'package:fasting_app/util/firebase_service.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:fasting_app/util/flash_helper.dart';
import 'package:fasting_app/models/bar_chart.dart';
import 'package:fasting_app/constants/colors.dart';
import 'package:fasting_app/constants/images.dart';
import 'package:fasting_app/constants/routes.dart';
import 'package:image_picker/image_picker.dart';
import 'package:fasting_app/util/app_utils.dart';
import 'package:fasting_app/models/models.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'dart:io';



class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {

  final FirebaseService firebaseUtil = GetIt.I.get<FirebaseService>();
  final AppUtils appUtils = GetIt.I.get<AppUtils>();
  final picker = ImagePicker();
  Color color;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: Container(
          decoration: BoxDecoration(gradient: AppColors.barGradient),
          child: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            title: Text('Profile'),
            centerTitle: true,
            actions: [
              Center(
                child: Padding(
                  padding: EdgeInsets.only(right: 10),
                  child: IconButton(
                    icon: Icon(Icons.settings),
                    onPressed: () => AppRoutes.push(context, Settings()),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              StreamBuilder<cloud.DocumentSnapshot>(
                stream: firebaseUtil.cloud.doc('Users/${firebaseUtil.user.uid}').snapshots(),
                builder: (context, snapUser) {
                  switch(snapUser.connectionState) {
                    case ConnectionState.waiting: return Center(
                      child: CircularProgressIndicator(),
                    );
                    default:
                      final user = UserDetails.fromJson(snapUser.data.data());
                      return Column(
                        children: [
                          Align(
                            alignment: Alignment.center,
                            child: Container(
                              alignment: Alignment.center,
                              margin: EdgeInsets.only(bottom: 15),
                              height: 180,
                              width: 180,
                              child: Stack(
                                children: [
                                  Container(
                                    height: 180,
                                    width: 180,
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: AppColors.lavander.withOpacity(.3)),
                                    child: Container(
                                      height: 150,
                                      width: 150,
                                      padding: EdgeInsets.all(10),
                                      margin: EdgeInsets.all(10),
                                      decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: AppColors.plum.withOpacity(.3)),
                                      child: ClipOval(
                                        child: CachedNetworkImage(
                                          imageUrl: "${user.userpic}",
                                          placeholder: (context, url) => Center(child: CircularProgressIndicator()),
                                          errorWidget: (context, url, error) => Center(child: Icon(Icons.error)),
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                    ),
                                  ),
                                  Positioned(
                                      right: 30,
                                      bottom: 0,
                                      child: CircleAvatar(
                                        radius: 20,
                                        backgroundColor: AppColors.greyBack,
                                        child: InkWell(
                                          onTap: () async {
                                            final pickedFile = await picker.getImage(source: ImageSource.gallery);
                                            if (pickedFile != null) {
                                              FlashHelper.successBar(context, message: 'Uploading photo...');
                                              await appUtils.updateImageProfile(context, File(pickedFile.path), user.userpic);
                                            }
                                          },
                                          child: Icon(
                                            Icons.edit,
                                            color: Colors.grey,
                                            size: 20,
                                          ),
                                        ),
                                      ))
                                ],
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.center,
                            child: Text(
                              '${user.username}',
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      );
                  }
                },
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    Images.gear,
                    scale: 5,
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Text(
                    '1 Achivenments',
                    style: TextStyle(
                        color: AppColors.plum,
                        fontSize: 18,
                        fontWeight: FontWeight.bold),
                  ),
                ],
              ),
              calculations(size),
              SizedBox(
                height: 20,
              ),
              Text(
                'Recent Fasts',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 20,
              ),
              FastingChart(
                data: data,
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                'Total Fasting Hours',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 20,
              ),
              TotalFastingChart(data: data),
              SizedBox(
                height: 20,
              ),
              Text(
                'Weight',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 20,
              ),
              WeightChart(data: data),
            ],
          ),
        ),
      ),
    );
  }

  calculations(size) {
    return Container(
      padding: EdgeInsets.only(left: 30, right: 30, top: 20, bottom: 20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                height: size.height / 7,
                width: size.width / 2.7,
                margin: EdgeInsets.only(top: 20),
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Color(0xFF8C85FF),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text('Total Fasts',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.bold)),
                    Text('0',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 35,
                            fontWeight: FontWeight.bold)),
                  ],
                ),
              ),
              Container(
                height: size.height / 7,
                width: size.width / 2.7,
                padding: EdgeInsets.all(10),
                margin: EdgeInsets.only(top: 20),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Color(0xFFFF7474),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    AutoSizeText('Average',
                      maxLines: 1,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    AutoSizeText('0',
                      maxLines: 1,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 35,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                height: size.height / 7,
                width: size.width / 2.7,
                padding: EdgeInsets.all(10),
                margin: EdgeInsets.only(top: 20),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Color(0xFF2AC4FF),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    AutoSizeText('Longest Fast',
                      maxLines: 1,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    AutoSizeText('0',
                      maxLines: 1,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 35,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                height: size.height / 7,
                width: size.width / 2.7,
                margin: EdgeInsets.only(top: 20),
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Color(0xFFD189CA),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    AutoSizeText('Longest Streak',
                        maxLines: 1,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.bold)),
                    AutoSizeText('0',
                        maxLines: 1,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 35,
                            fontWeight: FontWeight.bold)),
                  ],
                ),
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                height: size.height / 7,
                width: size.width / 2.7,
                padding: EdgeInsets.all(10),
                margin: EdgeInsets.only(top: 20),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Color(0xFFDD2A7B),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    AutoSizeText('Current Streak',
                      maxLines: 1,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    AutoSizeText('0',
                      maxLines: 1,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 35,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                height: size.height / 7,
                width: size.width / 2.7,
                margin: EdgeInsets.only(top: 20),
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Color(0xFF6BCC4F),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    AutoSizeText('Total Weight',
                        maxLines: 1,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.bold)),
                    AutoSizeText('0',
                        maxLines: 1,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 35,
                            fontWeight: FontWeight.bold)),
                  ],
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
