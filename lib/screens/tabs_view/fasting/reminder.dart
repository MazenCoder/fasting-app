import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:fasting_app/notifications/notifications.dart';
import 'package:fasting_app/database/app_database.dart';
import 'package:date_time_picker/date_time_picker.dart';
import 'package:fasting_app/widgets/picker_widget.dart';
import 'package:fasting_app/util/flash_helper.dart';
import 'package:fasting_app/constants/colors.dart';
import 'package:fasting_app/constants/routes.dart';
import 'package:fasting_app/util/app_utils.dart';
import 'package:fasting_app/widgets/appbar.dart';
import 'package:timezone/data/latest.dart' as tz;
import 'package:timezone/timezone.dart' as tz;
import 'package:provider/provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:jiffy/jiffy.dart';
import 'dart:math';
import 'dart:io';



DateTime setDate = DateTime.now();

class Reminders extends StatefulWidget {
  @override
  _RemindersState createState() => _RemindersState();
}

class _RemindersState extends State<Reminders> {

  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
  final Notifications _notifications = Notifications();
  final AppUtils appUtils = GetIt.I.get<AppUtils>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  DateTime dateTime = DateTime.now();
  bool status = false;

  final values = <bool>[false, false, false, false, false, false, false];
  int idDay = 1;



  Future<void> openTimePicker() async {
    await showTimePicker(context: context,
        initialTime: TimeOfDay.now(),
        helpText: "Choose Time").then((value) {
      DateTime newDate = DateTime(
          setDate.year,
          setDate.month,
          setDate.day,
          value != null ? value.hour : setDate.hour,
          value != null ? value.minute : setDate.minute);
      setState(() => setDate = newDate);
      print(newDate.hour);
      print(newDate.minute);
    });
  }

  Future<void> openDatePicker() async {
    await showDatePicker(
        context: context,
        initialDate: setDate,
        firstDate: DateTime.now(),
        lastDate: DateTime.now().add(Duration(days: 100000)))
        .then((value) {
      DateTime newDate = DateTime(
          value != null ? value.year : setDate.year,
          value != null ? value.month : setDate.month,
          value != null ? value.day : setDate.day,
          setDate.hour,
          setDate.minute);
      setState(() => setDate = newDate);
      print(setDate.day);
      print(setDate.month);
      print(setDate.year);
    });
  }


  @override
  void initState() {
    super.initState();
    initNotifies();
  }

  //init notifications
  Future initNotifies() async => flutterLocalNotificationsPlugin = await _notifications.initNotifies(context);


  Future _saveReminder([ReminderModel reminder]) async {
    int _id;

    if (reminder != null) {
      _id = reminder.idReminder;
    } else {
      _id = Random().nextInt(10000000);
    }

    print("setDate: $setDate");
    if (setDate.millisecondsSinceEpoch <= DateTime.now().millisecondsSinceEpoch) {
      print("Check your medicine time and date");
      FlashHelper.infoBar(context, message: 'Check your time and date');
    } else {
      //create pill object
      final model = ReminderModel(
        timeReminder: setDate.millisecondsSinceEpoch,
        idReminder: _id,
        date: setDate,
        title: DateFormat.yMMMEd().format(setDate),
        dateInsert: reminder != null ? reminder.dateInsert : DateTime.now(),
      );

      await appUtils.saveReminder(model);
      tz.initializeTimeZones();
      tz.setLocalLocation(tz.getLocation('Europe/Warsaw'));
      await _notifications.showNotification(model.title, "Fasting reminder", timeDifference, _id,
          flutterLocalNotificationsPlugin);
      FlashHelper.successBar(context, message: reminder != null ? 'reminder has been updated' : 'reminder has been sent');
      Navigator.pop(context);
    }
  }

  //get time difference
  int get timeDifference => setDate.millisecondsSinceEpoch - tz.TZDateTime.now(tz.local).millisecondsSinceEpoch;

  @override
  Widget build(BuildContext context) {
    final db = Provider.of<AppDatabase>(context);
    return Scaffold(
      key: _scaffoldKey,
      appBar: CustomAppBar(text: 'Set your reminders'),
      floatingActionButton: FloatingActionButton(
        onPressed: () => showModalBottomSheet(
          context: context,
          isScrollControlled: true,
          builder: (BuildContext context) {
            return StatefulBuilder(builder: (context, setState) {
              return Container(
                height: MediaQuery.of(context).size.height / 1.4,
                padding: EdgeInsets.all(15),
                color: Colors.white,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    GestureDetector(
                      onTap: () => AppRoutes.pop(context),
                      child: Align(
                        alignment: Alignment.topRight,
                        child: Container(
                          margin: EdgeInsets.only(bottom: 20),
                          height: 20,
                          width: 20,
                          //alignment: Alignment.topRight,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              border: Border.all(width: 2, color: Colors.grey)),
                          child: Icon(
                            Icons.clear,
                            size: 15,
                          ),
                        ),
                      ),
                    ),
                    Text('Choose Time',
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                    Card(
                        margin: EdgeInsets.only(top: 10, bottom: 10),
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                        color: AppColors.greyBack,
                        child: TimePickerWidget(),
                    ),
                    Text(
                      'Choose days',
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 8,),
                    PlatformFlatButton(
                      handler: () => openDatePicker(),
                      buttonChild: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            // SizedBox(width: 10),
                            Text(
                              DateFormat.yMMMEd().format(setDate),
                              style: TextStyle(
                                  fontSize: 20.0,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w500),
                            ),
                            SizedBox(width: 10),
                            Icon(
                              Icons.event,
                              size: 30,
                              color: Theme.of(context).primaryColor,
                            )
                          ],
                        ),
                      ),
                      color: Colors.purple.shade50,
                    ),
                    SizedBox(height: 25),

                    Align(
                      alignment: Alignment.center,
                      child: Container(
                        margin: EdgeInsets.only(bottom: 20),
                        width: 200,
                        child: RaisedButton(
                          onPressed: () async {
                            await _saveReminder();
                          },
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)),
                          color: AppColors.green,
                          child: Text(
                            'Save',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 18,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              );
            });
          },
        ),
        child: Container(
          height: double.infinity,
          width: double.infinity,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            gradient: AppColors.barGradient,
          ),
          child: Icon(
            Icons.add,
            size: 30,
            color: Colors.white,
          ),
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(15),
        child: StreamBuilder<List<ReminderModel>>(
          stream: db.reminderModelsDao.watchReminderByDate(),
          builder: (context, snapshot) {
            switch(snapshot.connectionState) {
              case ConnectionState.waiting: return Center(
                child: CircularProgressIndicator(),
              );
              default:
                final reminders = snapshot.data ?? [];
                return ListView.builder(
                    itemCount: reminders.length ?? 0,
                    itemBuilder: (context, index) {
                      final model = reminders[index];
                      final bool isEnd = DateTime.now().millisecondsSinceEpoch > model.timeReminder;
                      return Card(
                        shadowColor: AppColors.secondaryColor,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20)),
                        child: Container(
                            padding: EdgeInsets.all(10),
                            child: ListTile(
                              title: Text("${DateFormat('kk:mm:a').format(model.date)}",
                                style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 18,
                                  decoration: isEnd ? TextDecoration.lineThrough : null
                                ),
                              ),
                              subtitle: Text("${Jiffy(model.date).yMMMMd}"),
                              leading: IconButton(
                                icon: Icon(Icons.edit, color: Colors.purple),
                                onPressed: () => showModalBottomSheet(
                                  context: context,
                                  isScrollControlled: true,
                                  builder: (BuildContext context) {
                                    return StatefulBuilder(builder: (context, setState) {
                                      return Container(
                                        height: MediaQuery.of(context).size.height / 1.4,
                                        padding: EdgeInsets.all(15),
                                        color: Colors.white,
                                        child: Column(
                                          mainAxisSize: MainAxisSize.min,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: <Widget>[
                                            GestureDetector(
                                              onTap: () => AppRoutes.pop(context),
                                              child: Align(
                                                alignment: Alignment.topRight,
                                                child: Container(
                                                  margin: EdgeInsets.only(bottom: 20),
                                                  height: 20,
                                                  width: 20,
                                                  //alignment: Alignment.topRight,
                                                  decoration: BoxDecoration(
                                                      shape: BoxShape.circle,
                                                      border: Border.all(width: 2, color: Colors.grey)),
                                                  child: Icon(
                                                    Icons.clear,
                                                    size: 15,
                                                  ),
                                                ),
                                              ),
                                            ),
                                            Text('Choose Time',
                                              style:
                                              TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                                            ),
                                            Card(
                                              margin: EdgeInsets.only(top: 10, bottom: 10),
                                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                                              color: AppColors.greyBack,
                                              child: TimePickerWidget(),
                                            ),
                                            Text(
                                              'Choose days',
                                              style:
                                              TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                                            ),
                                            SizedBox(height: 8,),
                                            PlatformFlatButton(
                                              handler: () => openDatePicker(),
                                              buttonChild: Padding(
                                                padding: const EdgeInsets.all(8.0),
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  children: [
                                                    // SizedBox(width: 10),
                                                    Text(
                                                      DateFormat.yMMMEd().format(setDate),
                                                      style: TextStyle(
                                                          fontSize: 20.0,
                                                          color: Colors.black,
                                                          fontWeight: FontWeight.w500),
                                                    ),
                                                    SizedBox(width: 10),
                                                    Icon(
                                                      Icons.event,
                                                      size: 30,
                                                      color: Theme.of(context).primaryColor,
                                                    )
                                                  ],
                                                ),
                                              ),
                                              color: Colors.purple.shade50,
                                            ),
                                            SizedBox(height: 25),

                                            Align(
                                              alignment: Alignment.center,
                                              child: Container(
                                                margin: EdgeInsets.only(bottom: 20),
                                                width: 200,
                                                child: RaisedButton(
                                                  onPressed: () async {
                                                    await _saveReminder(model);
                                                  },
                                                  shape: RoundedRectangleBorder(
                                                      borderRadius: BorderRadius.circular(20)),
                                                  color: AppColors.green,
                                                  child: Text(
                                                    'Save',
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontSize: 18,
                                                        fontWeight: FontWeight.bold),
                                                  ),
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                      );
                                    });
                                  },
                                ),
                              ),
                              trailing: IconButton(
                                icon: Icon(Icons.delete, color: Colors.red),
                                onPressed: () async {
                                  await Notifications.removeNotify(model.idReminder, flutterLocalNotificationsPlugin);
                                  await db.reminderModelsDao.deleteReminder(model);
                                },
                              ),

                              // trailing: CustomSwitch(
                              //   activeColor: AppColors.primaryColor,
                              //   value: status,
                              //   onChanged: (value) {
                              //     print("VALUE : $value");
                              //     setState(() {
                              //       status = value;
                              //     });
                              //   },
                              // ),
                            )),
                      );
                    },
                );
            }
          },
        ),
      ),
    );
  }
}


class PlatformFlatButton extends StatelessWidget {
  final Function handler;
  final Widget buttonChild;
  final Color color;

  PlatformFlatButton({@required this.buttonChild,@required this.color,@required this.handler});

  @override
  Widget build(BuildContext context) {
    return Platform.isIOS
        ? CupertinoButton(
      child: this.buttonChild,
      color: this.color,
      onPressed: this.handler,
      borderRadius: BorderRadius.circular(15.0),
    )
        : FlatButton(
      color: this.color,
      child: this.buttonChild,
      onPressed: this.handler,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0)),
    );
  }
}
