import 'package:material_dialogs/widgets/buttons/icon_button.dart';
import 'package:flutter_sparkline/flutter_sparkline.dart';
import 'package:fasting_app/util/firebase_service.dart';
import 'package:fasting_app/util/flash_helper.dart';
import 'package:fasting_app/mobx/mobx_fasting.dart';
import 'package:fasting_app/constants/colors.dart';
import 'package:fasting_app/constants/images.dart';
import 'package:fasting_app/constants/routes.dart';
import 'package:fasting_app/util/app_utils.dart';
import 'package:fasting_app/models/journal.dart';
import 'package:fasting_app/models/models.dart';
import 'package:flutter_dash/flutter_dash.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:jiffy/jiffy.dart';
import 'package:uuid/uuid.dart';
import '../../../constants.dart';
import 'fasting.dart';


class EndFast extends StatefulWidget {

  final UserDetails details;
  EndFast(this.details);

  @override
  _EndFastState createState() => _EndFastState();
}

class _EndFastState extends State<EndFast> {

  final FirebaseService firebaseUtil = GetIt.I.get<FirebaseService>();
  final AppUtils appUtils = GetIt.I.get<AppUtils>();
  final MobxFasting _mobx = MobxFasting();
  final _form = GlobalKey<FormState>();
  final now = DateTime.now();

  final TextEditingController controllerNote = TextEditingController();
  final TextEditingController controllerWeight = TextEditingController();
  var data = [0.0, 1.0, 1.5, 2.0, 0.0, 0.0, -0.5, -1.0, -0.5, 0.0, 0.0];


  @override
  Widget build(BuildContext context) {
    var remaining = now.difference(start);
    final size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(150),
        child: Container(
          decoration: BoxDecoration(gradient: AppColors.barGradient),
          child: SafeArea(
              child: Container(
            padding: EdgeInsets.all(10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Nice Effort !',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontWeight: FontWeight.bold),
                ),
                Text(
                  'You completed a fast fot a total of ${appUtils.endFormatDuration(remaining)}.',
                  textScaleFactor: 1.5,
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
                Row(
                  children: [
                    Icon(
                      Icons.share,
                      color: Colors.white,
                      size: 20,
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Text(
                      'Share Fast',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                          fontStyle: FontStyle.italic),
                    )
                  ],
                )
              ],
            ),
          )),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(15),
          child: Column(
            children: [
              Container(
                height: 70,
                width: double.infinity,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text(
                      'Started Fasting',
                      style: TextStyle(
                          color: AppColors.darkBlue,
                          fontSize: 18,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      '${Jiffy(widget.details.fastStartedAt).format("MMMM do yyyy, h:mm:ss a",)}',
                      style: TextStyle(
                        fontSize: 18,
                      ),
                    ),
                    Dash(
                      length: size.width - 30,
                      dashGap: 8,
                      dashLength: 10,
                    )
                  ],
                ),
              ),
              Container(
                height: 70,
                width: double.infinity,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text(
                      'Goal Reached',
                      style: TextStyle(
                          color: AppColors.darkBlue,
                          fontSize: 18,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      '${Jiffy(now).format("MMMM do yyyy, h:mm:ss a",)}',
                      style: TextStyle(
                        fontSize: 18,
                      ),
                    ),
                    Dash(
                      length: size.width - 30,
                      dashGap: 8,
                      dashLength: 10,
                    )
                  ],
                ),
              ),
              // JournalAdd(
              //   pop: false,
              //   button: false,
              //   userDetails: widget.details,
              // ),

              Form(
                key: _form,
                child: Column(
                  children: [
                    SizedBox(height: 18),
                    Text(
                      'Update your Journal',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                      ),
                    ),
                    Card(
                      margin: EdgeInsets.only(top: 15, bottom: 15),
                      color: AppColors.greyBack,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                      child: Container(
                        padding: EdgeInsets.all(8),
                        child: Column(
                          children: [
                            Text(
                              'How are you feeling? ',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 18),
                            ),
                            Wrap(
                              alignment: WrapAlignment.center,
                              children: feelingOptions.entries.map((e) {
                                return Observer(
                                  builder: (_) {
                                    return Material(
                                      borderRadius: BorderRadius.circular(100),
                                      color: e.key == _mobx.idFeelingOption ? AppColors.secondaryColor : null,
                                      child: Padding(
                                        padding: const EdgeInsets.all(1),
                                        child: IconButton(
                                          icon: ImageIcon(
                                            AssetImage(
                                              e.value,
                                            ),
                                            color: e.key == _mobx.idFeelingOption ?
                                            Colors.white : AppColors.secondaryColor,
                                          ),
                                          onPressed: () {
                                            print(e.key);
                                            _mobx.setIdFeeling(e.key);
                                          },
                                        ),
                                      ),
                                    );
                                  },
                                );
                              }).toList(),
                            )
                          ],
                        ),
                      ),
                    ),

                    Card(
                      margin: EdgeInsets.only(bottom: 15),
                      color: AppColors.greyBack,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: Container(
                        padding: EdgeInsets.all(10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('Mood Graph',
                              style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold,
                              ),
                            ),

                            Stack(
                              children: <Widget>[
                                AspectRatio(
                                  aspectRatio: 1.15,
                                  child: Container(
                                    decoration: const BoxDecoration(
                                      // color: Colors.amber,
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(18),
                                      ),
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                        right: 40.0, left: 12.0, top: 24, bottom: 12,
                                      ),
                                      child: FutureBuilder<List<double>>(
                                        future: appUtils.getSparklineData(),
                                        builder: (context, snapshot) {
                                          switch(snapshot.connectionState) {
                                            case ConnectionState.waiting: return Center(
                                              child: CircularProgressIndicator(),
                                            );
                                            default:

                                              if (snapshot.data == null ||
                                                  snapshot.data.isEmpty ||
                                                snapshot.data.length == 1) {
                                                return Container();
                                              }

                                              return Sparkline(
                                                data: snapshot.data,
                                                lineColor: Colors.purple,
                                                pointsMode: PointsMode.all,
                                                pointSize: 8.0,
                                              );
                                          }
                                        },
                                      )

                                    ),
                                  ),
                                ),

                                Positioned(
                                  right: 0,
                                  child: Container(
                                    height: 300,
                                    child: Column(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: feelingOptions.entries.map((e) {
                                        return ImageIcon(
                                          AssetImage(
                                            e.value,
                                          ),
                                        );
                                      }).toList(),
                                    ),
                                  ),
                                ),
                              ],
                            )

                          ],
                        ),
                      ),
                    ),
                    Card(
                      margin: EdgeInsets.only(bottom: 15),
                      color: AppColors.greyBack,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: Container(
                        padding: EdgeInsets.all(10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('Add a Note',
                              style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold,
                              ),
                            ),

                            Container(
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10),
                              ),
                              child: TextField(
                                maxLines: 5,
                                controller: controllerNote,
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.symmetric(horizontal: 7),
                                  border: InputBorder.none,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    Card(
                      margin: EdgeInsets.only(bottom: 15),
                      color: AppColors.greyBack,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                      child: Container(
                        padding: EdgeInsets.all(10),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('Add Weight',
                                style: TextStyle(
                                    fontSize: 18, fontWeight: FontWeight.bold),
                              ),
                              Container(
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                child: TextFormField(
                                  //maxLines: 5,
                                  controller: controllerWeight,
                                  keyboardType: TextInputType.number,
                                  decoration: InputDecoration(
                                    contentPadding:
                                    EdgeInsets.symmetric(horizontal: 7),
                                    border: InputBorder.none,
                                  ),
                                  validator: (val) {
                                    if(val.isEmpty) {
                                      return 'Required field';
                                    } else return null;
                                  },
                                ),
                              )
                            ]),
                      ),
                    ),
                  ],
                ),
              ),

              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    onTap: () => AppRoutes.pop(context),
                    child: Container(
                      height: 40,
                      width: 150,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: Colors.white,
                          border: Border.all(color: AppColors.green, width: 2)),
                      child: RaisedButton(
                        color: Colors.white,
                        elevation: 0,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20)),
                        onPressed: () => Navigator.pop(context, true),
                        child: Text('Delete',
                          style: TextStyle(
                              color: AppColors.green,
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height: 40,
                    width: 150,
                    child: RaisedButton(
                      onPressed: () async {
                        if (_form.currentState.validate()) {
                          FocusScope.of(context).unfocus();
                          final id = Uuid().v4();
                          final journal = Journal(
                            id: id,
                            uid: firebaseUtil.user.uid,
                            staredFasting: widget.details.fastStartedAt,
                            goalFasting: DateTime.now().toString(),
                            idFeeling: _mobx.idFeelingOption,
                            weight: controllerWeight.text.trim(),
                            note: controllerNote.text.trim(),
                          );
                          FlashHelper.successBar(context, message: 'The journal has been updated');
                          await appUtils.addJournal(journal, id);
                          await showDialog(context: context, builder: (context) {
                            return AlertDialog(
                              actionsPadding: const EdgeInsets.all(0),
                              buttonPadding: const EdgeInsets.all(0),
                              // contentPadding: const EdgeInsets.all(0),
                              titlePadding: const EdgeInsets.all(0),
                              title: Image.asset(
                                'assets/images/Congratulations.gif',
                                fit: BoxFit.fill,
                                height: 150,
                              ),
                              content: Container(
                                // height: 250,
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text('Congratulations',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    SizedBox(height: 5,),
                                    Text('Congratulations, you achieved your goal\n',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        fontSize: 14,
                                      ),
                                    ),
                                    Container(
                                      child: Column(
                                        children: [
                                          Container(
                                            decoration: BoxDecoration(
                                                shape: BoxShape.circle,
                                                border: Border.all(
                                                  color: AppColors.orange,
                                                  width: 4,
                                                )),
                                            padding: EdgeInsets.all(8),
                                            child: Column(
                                              children: [
                                                Icon(
                                                  Icons.star,
                                                  color: Colors.yellow[700],
                                                  size: 25,
                                                ),
                                                Image.asset(
                                                  Images.one,
                                                  scale: 3,
                                                  height: 90,
                                                ),
                                              ],
                                            ),
                                          ),
                                          IconsButton(
                                            onPressed: () => Navigator.pop(context),
                                            text: 'Claim',
                                            iconData: Icons.done,
                                            color: AppColors.primaryColor,
                                            textStyle: TextStyle(color: Colors.white),
                                            iconColor: Colors.white,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            );
                          }).then((value) {
                            Navigator.pop(context, true);
                          });
                        }
                      },
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                      color: AppColors.green,
                      child: Text('Save',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 30,),
            ],
          ),
        ),
      ),
    );
  }
}
