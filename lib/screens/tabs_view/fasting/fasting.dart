import 'package:fasting_app/screens/tabs_view/fasting/reminder.dart';
import 'package:sleek_circular_slider/sleek_circular_slider.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_sparkline/flutter_sparkline.dart';
import 'package:fasting_app/util/firebase_service.dart';
import 'package:fasting_app/widgets/target_widget.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fasting_app/widgets/add_journal.dart';
import 'package:fasting_app/mobx/mobx_fasting.dart';
import 'package:fasting_app/util/flash_helper.dart';
import 'package:fasting_app/constants/colors.dart';
import 'package:fasting_app/constants/images.dart';
import 'package:fasting_app/constants/routes.dart';
import 'package:timer_builder/timer_builder.dart';
import 'package:fasting_app/models/journal.dart';
import 'package:fasting_app/util/app_utils.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:fasting_app/models/models.dart';
import 'package:fasting_app/widgets/share.dart';
import 'package:flutter/material.dart';
import 'choose_plan/choose_plan.dart';
import 'package:get_it/get_it.dart';
import 'package:jiffy/jiffy.dart';
import '../../../constants.dart';
import 'package:uuid/uuid.dart';
import 'package:time/time.dart';
import 'end_fast.dart';


final FirebaseService firebaseUtil = GetIt.I.get<FirebaseService>();

int hour = 1;
DateTime start;
DateTime end;

bool planSelected = false;
bool fastStarted = false;

int endTime = DateTime.now().millisecondsSinceEpoch + 1000 * 60 * 60 * hour;
int endTime2 = DateTime.now().millisecondsSinceEpoch + 1000 * 60 * 60 * hour;


void onEnd() async {
  await firebaseUtil.cloud.doc('Users/${firebaseUtil.user.uid}')
      .update({"active": false});
}

class FastingScreen extends StatefulWidget {
  @override
  _FastingScreenState createState() => _FastingScreenState();
}

class _FastingScreenState extends State<FastingScreen> {



  TextEditingController _weightController = TextEditingController();
  final AppUtils appUtils = GetIt.I.get<AppUtils>();
  final MobxFasting _mobx = MobxFasting();
  var _formKey = GlobalKey<FormState>();
  final now = DateTime.now();
  bool _isLoading = false;
  Size size;


  @override
  void dispose() {
    _weightController.dispose();
    super.dispose();
  }

  void startTimer() async {
    const oneSec = const Duration(hours: 1);

    await firebaseUtil.cloud.doc('Users/${firebaseUtil.user.uid}')
        .update({
      "userWeight": _weightController.text.trim(),
      "fastingHoursLeft": endTime.toString(),
      "fastStartedAt": DateTime.now().toString(),
      "fastStartTime": DateTime.now().toString(),
      "fastEndTime": Jiffy(DateTime.now()).add(hours: hour).toString(),
      "active": true,
    });
  }

  void _onFinish() async {
    await firebaseUtil.cloud.doc('Users/${firebaseUtil.user.uid}')
        .update({"active": false});
  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Get Ready to Fast'),
        flexibleSpace: Container(
          decoration: BoxDecoration(gradient: AppColors.barGradient),
        ),
      ),
      body: FutureBuilder<UserDetails>(
        future: appUtils.getUserData(),
        builder: (context, snapshot) {
          switch(snapshot.connectionState) {
            case ConnectionState.waiting: return Center(
              child: CircularProgressIndicator(),
            );
            default:
              final _userDetails = snapshot.data;

              if (_userDetails.active ?? false) {

                hour = int.parse(_userDetails.fastingHour);
                start = DateTime.parse(_userDetails.fastStartTime);
                end = DateTime.parse(_userDetails.fastEndTime);

                final ended = now.compareTo(end) >= 0;
                var remaining = now.difference(start);

                print('ended: ${(remaining.inHours/hour*100).toInt()}');
                final _initTimer = (remaining.inHours/hour*100).toInt();

                if (_initTimer < 100) {
                  fastStarted = _userDetails.active ?? false;
                  _mobx.setTimer(_initTimer);
                } else {
                  fastStarted = false;
                  _mobx.setTimer(0);
                  _onFinish();
                }
              }

              return SingleChildScrollView(
                child: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      GestureDetector(
                        onTap: () => AppRoutes.push(context, ChoosePlan()),
                        child: Container(
                          width: 150,
                          margin: EdgeInsets.only(top: 15, bottom: 20),
                          alignment: Alignment.center,
                          padding: EdgeInsets.all(5),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            border: Border.all(color: AppColors.green, width: 2),
                          ),
                          child: Text(
                            'Choose Plan',
                            style: TextStyle(
                              color: AppColors.green,
                            ),
                          ),
                        ),
                      ),
                      SleekCircularSlider(
                        initialValue: _mobx.timer + 0.0,
                        innerWidget: (val) {
                          return Center(
                            child: SizedBox(
                              child: CircleAvatar(
                                radius: size.width / 3,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(top: 8.0),
                                      child: Text(
                                        'Time since Last Fast\n',
                                        style: TextStyle(
                                          fontSize: 18, fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                    fastStarted ?

                                    TimerBuilder.periodic(
                                        Duration(seconds: 1),
                                        alignment: Duration.zero,
                                        builder: (context) {
                                          var now = DateTime.now();
                                          var remaining = now.difference(start);
                                          final Duration oneHourThirtyMinutes = hour.hours - remaining;
                                          // _mobx.setTimer((remaining.inHours/hour*100).toInt());
                                          return Text("${appUtils.formatDuration(oneHourThirtyMinutes)}",
                                            style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
                                          );
                                        }
                                    ) : Text("00:00:00",
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Text(
                                      'Up coming fast\n',
                                      style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),

                                    fastStarted ?
                                    Text('$hour hour',
                                      style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ) : Text(
                                      '0 hour',
                                      style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          );
                        },
                        appearance: CircularSliderAppearance(
                            customWidths: CustomSliderWidths(
                                handlerSize: 20, trackWidth: 5, progressBarWidth: 12),
                            size: size.width / 1.2,
                            customColors: CustomSliderColors(
                                gradientStartAngle: 0,
                                dotColor: Color(0xff3E28B6),
                                gradientEndAngle: 270,
                                progressBarColors: AppColors.barGradient.colors)),
                        onChange: (val) {},
                      ),
                      fastStarted ? TargetWidget() : Container(),
                      fastStarted ? endFast(_userDetails) : Container(
                        width: 200,
                        margin: EdgeInsets.only(bottom: 20),
                        child: RaisedButton(
                          onPressed: () {
                            if (planSelected) showDialog(
                                  context: context,
                                  builder: (_) => Dialog(
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(20),
                                    ),
                                    child: Container(
                                      padding: EdgeInsets.all(10),
                                      //height: 150,
                                      child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [

                                          Text('Log Weight\n ',
                                            style: TextStyle(
                                              fontSize: 18,
                                              fontWeight:
                                              FontWeight.bold,
                                            ),
                                          ),

                                          Card(
                                            margin:
                                            EdgeInsets.only(bottom: 15),
                                            color: AppColors.greyBack,
                                            shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.circular(20),
                                            ),
                                            child: Container(
                                              padding: EdgeInsets.all(10),
                                              child: Form(
                                                key: _formKey,
                                                child: Column(
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: [
                                                    Text('Add Weight',
                                                      style: TextStyle(
                                                        fontSize: 18,
                                                        fontWeight: FontWeight.bold,
                                                      ),
                                                    ),

                                                    Container(
                                                      decoration: BoxDecoration(
                                                        color: Colors.white,
                                                        borderRadius: BorderRadius.circular(10),
                                                      ),
                                                      child: TextFormField(
                                                        validator: (val) {
                                                          if (val.length == 0) {
                                                              return "Weight cannot be empty";
                                                            } else {
                                                              return null;
                                                            }
                                                          },
                                                          controller: _weightController,
                                                          //maxLines: 5,//maxLines: 5,
                                                          keyboardType: TextInputType.number,
                                                          decoration: InputDecoration(
                                                            contentPadding: EdgeInsets.symmetric(horizontal: 7),
                                                            border: InputBorder.none,
                                                          ),
                                                        ),
                                                      )
                                                    ]),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            child: _isLoading ? Center(
                                              child: CircularProgressIndicator(),
                                            ) : RaisedButton(shape:
                                              RoundedRectangleBorder(
                                                borderRadius: BorderRadius.circular(20),
                                              ),
                                              onPressed: () async {
                                                if (_formKey.currentState.validate()) {
                                                  setState(() {
                                                    _isLoading = true;
                                                    fastStarted = true;
                                                    startTimer();
                                                  });

                                                  setState(() {
                                                    _isLoading = false;
                                                  });

                                                  Navigator.pop(context);
                                                }
                                              },
                                              textColor: Colors.white,
                                              color: AppColors.green,
                                              child: Text('ok'),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ));
                            else {
                              showDialog(
                                  context: context,
                                  child: AlertDialog(
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                        new BorderRadius.circular(18.0),
                                        side: BorderSide(
                                          color: Colors.purple,
                                        )),
                                    title: Text("Kindly first choose your plan"),
                                    actions: <Widget>[
                                      FlatButton(
                                        child: Text(
                                          "OK",
                                          style: TextStyle(color: Colors.purple),
                                        ),
                                        onPressed: () {
                                          Navigator.pop(context);
                                        },
                                      )
                                    ],
                                  ));
                              setState(() {});
                            }
                            // fastStarted = true;
                          },
                          color: AppColors.green,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)),
                          textColor: Colors.white,
                          child: Text('Start Your Fast'),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(bottom: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            GestureDetector(
                              onTap: () => AppRoutes.push(context, ChoosePlan()),
                              child: Container(
                                width: 150,
                                alignment: Alignment.center,
                                padding: EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(20),
                                    border:
                                    Border.all(color: AppColors.green, width: 2)),
                                child: Text(
                                  'Change Fast',
                                  style: TextStyle(
                                    color: AppColors.green,
                                  ),
                                ),
                              ),
                            ),
                            GestureDetector(
                              onTap: () => AppRoutes.push(context, Reminders()),
                              child: Container(
                                width: 150,
                                alignment: Alignment.center,
                                padding: EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20),
                                  border: Border.all(color: AppColors.green, width: 2),
                                ),
                                child: Text(
                                  'Set Reminder',
                                  style: TextStyle(
                                    color: AppColors.green,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 150,
                        alignment: Alignment.bottomCenter,
                        padding: EdgeInsets.all(10),
                        child: StreamBuilder<QuerySnapshot>(
                          stream: firebaseUtil.cloud.collection('Users')
                              .where('active', isEqualTo: true).snapshots(),
                          builder: (context, snapshot) {
                            List<QueryDocumentSnapshot> docs = snapshot.data?.docs ?? [];

                            if (fastStarted && docs.length -1 == 0) {
                              return Container();
                            } else if (!fastStarted && docs.isEmpty) {
                              return Container();
                            }
                            return Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: Text('Fasting Now',
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                      fontSize: 18,
                                      color: AppColors.lightPurple,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),

                                Expanded(
                                  child: ListView.builder(
                                    itemCount: docs.length,
                                    scrollDirection: Axis.horizontal,
                                    itemBuilder: (BuildContext context, int index) {
                                      final user = UserDetails.fromJson(docs[index].data());
                                      if (user.userUid.contains(firebaseUtil.user.uid)) {
                                        return Container();
                                      } else return Container(
                                        width: 95, height: 95,
                                        padding: const EdgeInsets.all(5),
                                        child: ClipRRect(
                                          borderRadius: new BorderRadius.circular(100.0),
                                          child: CachedNetworkImage(
                                            width: 95, height: 95,
                                            imageUrl: "${user.userpic ?? ''}",
                                            placeholder: (context, url) => Center(child: CircularProgressIndicator()),
                                            errorWidget: (context, url, error) => Center(child: Icon(Icons.error)),
                                            fit: BoxFit.fill,
                                          ),
                                        ),
                                      );
                                    },
                                  ),
                                ),
                              ],
                            );
                          },
                        )
                      )
                    ],
                  ),
                ),
              );
          }
        },
      )
    );
  }

  Widget endFast(UserDetails details) {

    final FirebaseService firebaseUtil = GetIt.I.get<FirebaseService>();
    final AppUtils appUtils = GetIt.I.get<AppUtils>();
    final MobxFasting _mobx = MobxFasting();
    final _form = GlobalKey<FormState>();

    final TextEditingController controllerNote = TextEditingController();
    final TextEditingController controllerWeight = TextEditingController();

    return Padding(
      padding: EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          GestureDetector(
            onTap: () => showModalBottomSheet<dynamic>(
                isScrollControlled: true,
                backgroundColor: Colors.transparent,
                context: context,
                builder: (context) {
                  return SingleChildScrollView(
                    // physics: NeverScrollableScrollPhysics(),
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(40),
                          topRight: Radius.circular(40),
                        ),
                      ),
                      child: Padding(
                        padding: EdgeInsets.fromLTRB(15, 30, 15, 0.0),
                        child: Form(
                          key: _form,
                          child: Column(
                            children: [
                              GestureDetector(
                                onTap: () => AppRoutes.pop(context),
                                child: Align(
                                  alignment: Alignment.topRight,
                                  child: Container(
                                    margin: EdgeInsets.only(bottom: 20),
                                    height: 20,
                                    width: 20,
                                    //alignment: Alignment.topRight,
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        border: Border.all(width: 2, color: Colors.grey)),
                                    child: Icon(
                                      Icons.clear,
                                      size: 15,
                                    ),
                                  ),
                                ),
                              ),

                              Text(
                                'Update your Journal',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20,
                                ),
                              ),
                              Card(
                                margin: EdgeInsets.only(top: 15, bottom: 15),
                                color: AppColors.greyBack,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20)),
                                child: Container(
                                  padding: EdgeInsets.all(8),
                                  child: Column(
                                    children: [
                                      Text('How are you feeling? ',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold, fontSize: 18),
                                      ),
                                      Wrap(
                                        alignment: WrapAlignment.center,
                                        children: feelingOptions.entries.map((e) {
                                          return Observer(
                                            builder: (_) {
                                              return Material(
                                                borderRadius: BorderRadius.circular(100),
                                                color: e.key == _mobx.idFeelingOption ?
                                                  AppColors.secondaryColor : null,
                                                child: Padding(
                                                  padding: const EdgeInsets.all(1),
                                                  child: IconButton(
                                                    icon: ImageIcon(
                                                      AssetImage(
                                                        e.value,
                                                      ),
                                                      color: e.key == _mobx.idFeelingOption ?
                                                      Colors.white : AppColors.secondaryColor,
                                                    ),
                                                    onPressed: () {
                                                      print(e.key);
                                                      _mobx.setIdFeeling(e.key);
                                                    },
                                                  ),
                                                ),
                                              );
                                            },
                                          );
                                        }).toList(),
                                      )
                                    ],
                                  ),
                                ),
                              ),

                              Card(
                                margin: EdgeInsets.only(bottom: 15),
                                color: AppColors.greyBack,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20),
                                ),
                                child: Container(
                                  padding: EdgeInsets.all(10),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text('Mood Graph',
                                        style: TextStyle(
                                          fontSize: 18, fontWeight: FontWeight.bold,
                                        ),
                                      ),

                                      Stack(
                                        children: <Widget>[
                                          AspectRatio(
                                            aspectRatio: 1.15,
                                            child: Container(
                                              decoration: const BoxDecoration(
                                                // color: Colors.amber,
                                                borderRadius: BorderRadius.all(
                                                  Radius.circular(18),
                                                ),
                                              ),
                                              child: Padding(
                                                padding: const EdgeInsets.only(
                                                  right: 40.0, left: 12.0, top: 24, bottom: 12,
                                                ),
                                                child: FutureBuilder<List<double>>(
                                                  future: appUtils.getSparklineData(),
                                                  builder: (context, snapshot) {
                                                    switch(snapshot.connectionState) {
                                                      case ConnectionState.waiting: return Center(
                                                        child: CircularProgressIndicator(),
                                                      );
                                                      default:

                                                        if (snapshot.data == null ||
                                                            snapshot.data.isEmpty ||
                                                            snapshot.data.length == 1) {
                                                          return Container();
                                                        }

                                                        return Sparkline(
                                                          data: snapshot.data,
                                                          lineColor: Colors.purple,
                                                          pointsMode: PointsMode.all,
                                                          pointSize: 8.0,
                                                        );
                                                    }
                                                  },
                                                ),
                                              ),
                                            ),
                                          ),

                                          Positioned(
                                            right: 0,
                                            child: Container(
                                              height: 300,
                                              child: Column(
                                                mainAxisSize: MainAxisSize.max,
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: feelingOptions.entries.map((e) {
                                                  return ImageIcon(
                                                    AssetImage(
                                                      e.value,
                                                    ),
                                                  );
                                                }).toList(),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Card(
                                margin: EdgeInsets.only(bottom: 15),
                                color: AppColors.greyBack,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20),
                                ),
                                child: Container(
                                  padding: EdgeInsets.all(10),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text('Add a Note',
                                        style: TextStyle(
                                          fontSize: 18, fontWeight: FontWeight.bold,
                                        ),
                                      ),

                                      Container(
                                        decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius: BorderRadius.circular(10),
                                        ),
                                        child: TextField(
                                          maxLines: 5,
                                          controller: controllerNote,
                                          decoration: InputDecoration(
                                            contentPadding: EdgeInsets.symmetric(horizontal: 7),
                                            border: InputBorder.none,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Card(
                                margin: EdgeInsets.only(bottom: 15),
                                color: AppColors.greyBack,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20),
                                ),
                                child: Container(
                                  padding: EdgeInsets.all(10),
                                  child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text('Add Weight',
                                          style: TextStyle(
                                              fontSize: 18, fontWeight: FontWeight.bold),
                                        ),
                                        Container(
                                          decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: BorderRadius.circular(10),
                                          ),
                                          child: TextFormField(
                                            //maxLines: 5,
                                            controller: controllerWeight,
                                            keyboardType: TextInputType.number,
                                            decoration: InputDecoration(
                                              contentPadding:
                                              EdgeInsets.symmetric(horizontal: 7),
                                              border: InputBorder.none,
                                            ),
                                            validator: (val) {
                                              if(val.isEmpty) {
                                                return 'Required field';
                                              } else return null;
                                            },
                                          ),
                                        )
                                      ]),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(bottom: 20),
                                width: 200,
                                child: RaisedButton(
                                  onPressed: () async {
                                    if (_form.currentState.validate()) {
                                      FocusScope.of(context).unfocus();
                                      final id = Uuid().v4();
                                      final journal = Journal(
                                        id: id,
                                        uid: firebaseUtil.user.uid,
                                        staredFasting: details.fastStartedAt,
                                        goalFasting: DateTime.now().toString(),
                                        idFeeling: _mobx.idFeelingOption,
                                        weight: controllerWeight.text.trim(),
                                        note: controllerNote.text.trim(),
                                      );
                                      controllerWeight.clear();
                                      controllerNote.clear();
                                      Navigator.pop(context);
                                      FlashHelper.successBar(
                                        context, message: 'The journal has been updated',
                                      );
                                      appUtils.addJournal(journal, id);
                                    }
                                  },
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(20)),
                                  color: AppColors.green,
                                  child: Text('Save',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                              SizedBox(height: size.height/2.5),
                            ],
                          ),
                        ),
                      ),
                    ),
                  );
                  return JournalAdd(
                    pop: true,
                    button: true,
                    userDetails: details,
                  );
                }),
            child: ImageIcon(
              AssetImage(Images.happyFace),
              color: AppColors.darkPink,
              size: 30,
            ),
          ),
          Container(
            width: 200,
            child: RaisedButton(
              onPressed: () async {
                bool isFinished = await Navigator.push(context, MaterialPageRoute(
                  builder: (context) => EndFast(details),
                ));
                if (isFinished ?? false) {
                  _onFinish();
                  setState(() {
                    fastStarted = false;
                    endTime = 0;
                  });
                }
              },
              color: AppColors.green,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              textColor: Colors.white,
              child: Text('End Fast'),
            ),
          ),
          GestureDetector(
              onTap: () => showModalBottomSheet<dynamic>(
                    isScrollControlled: true,
                    backgroundColor: Colors.transparent,
                    context: context,
                    builder: (contex) => ShareWidget(
                      size: size,
                      context: context,
                      details: details,
                      initCircle: _mobx.timer,
                    ),
                  ),
              child: Icon(
                Icons.share,
                color: AppColors.darkPink,
                size: 30,
              )),
        ],
      ),
    );
  }
}
