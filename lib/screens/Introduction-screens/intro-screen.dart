import 'package:fasting_app/screens/Introduction-screens/plan_selection.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:fasting_app/screens/tabs_view/tab_views.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fasting_app/widgets/picker_widget.dart';
import 'package:fasting_app/util/firebase_service.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fasting_app/constants/colors.dart';
import 'package:fasting_app/constants/images.dart';
import 'package:fasting_app/constants/routes.dart';
import 'package:fasting_app/util/app_utils.dart';
import 'package:fasting_app/widgets/appbar.dart';
import 'package:fasting_app/models/models.dart';
import 'package:fasting_app/Auth/Register.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'fasting_time.dart';
import 'summary.dart';
import 'dart:convert';
import 'goals.dart';
import 'dart:io';



class Introduction extends StatefulWidget {
  final File imageFile;
  Introduction(this.imageFile);

  @override
  _IntroductionState createState() => _IntroductionState(imageFile);
}

class _IntroductionState extends State<Introduction> {
  final File imageFile;
  _IntroductionState(this.imageFile);

  final FirebaseService firebaseUtil = GetIt.I.get<FirebaseService>();
  final AppUtils appUtils = GetIt.I.get<AppUtils>();

  int page = 0;
  bool _isLoading = false;
  List<PageViewModel> getPages() {
    return [
      PageViewModel(
        image: Image.asset(Images.intro1),
        title: 'The Newest Health and\n Wellness App',
        body: "To make your own fasting plan, we'd prefer to become acquainted with you better. your information will stay private.\n\nFast with your friends, share your fast, get motivated and complete challanges along with your buddies\n\nFasting has been shown to have many health benefits, from increased weight loss to better brain function.Promotes Better Health by Fighting Inflammation",
        decoration: const PageDecoration(
            bodyTextStyle:
                TextStyle(fontSize: 18.0, color: AppColors.lightPurple),
            titleTextStyle: TextStyle(
              fontSize: 25,
              fontWeight: FontWeight.bold,
            )),
      ),
      PageViewModel(
        title: 'What can we help you \n accomplish?',
        bodyWidget: Container(
          padding: EdgeInsets.all(05),
          // color: Colors.green,

          child: Goals(),
        ),
        decoration: const PageDecoration(
            bodyTextStyle: TextStyle(fontSize: 15.0),
            titleTextStyle: TextStyle(
              fontSize: 25,
              fontWeight: FontWeight.bold,
            )),
      ),
      PageViewModel(
        title: 'What, when, and how\nmuch we eat matters',
        bodyWidget: FastingTime(),
        decoration: const PageDecoration(
            bodyTextStyle: TextStyle(fontSize: 16.0, color: Color(0xff3F5261)),
            titleTextStyle: TextStyle(
              fontSize: 25,
              fontWeight: FontWeight.bold,
            )),
      ),
      PageViewModel(
        title: "",
        bodyWidget: PlanSelection(),
        decoration: const PageDecoration(
            bodyTextStyle: TextStyle(fontSize: 16.0, color: Color(0xff3F5261)),
            titleTextStyle: TextStyle(
              fontSize: 25,
              fontWeight: FontWeight.bold,
            )),
      ),
      PageViewModel(
        title: 'You' 're ready to get\n    started!',
        image: Image.asset(
          'assets/images/summary-image.png',
          scale: 1.5,
        ),
        bodyWidget: Summary(),
        decoration: const PageDecoration(
            bodyTextStyle: TextStyle(fontSize: 16.0, color: Color(0xff3F5261)),
            titleTextStyle: TextStyle(
              fontSize: 25,
              fontWeight: FontWeight.bold,
            )),
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: CustomAppBar(text: 'Welcome to fasting'),
        body: IntroductionScreen(
          pages: getPages(),
          dotsDecorator: DotsDecorator(
            color: AppColors.pink, activeColor: AppColors.darkBlue),
          onDone: () {},
          next: const Icon(Icons.navigate_next),
          done: Container(
            child: _isLoading ? CircularProgressIndicator()
                : RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadiusDirectional.circular(20),
                    ),
                    onPressed: () async {
                      print(2);
                      setState(() {
                        _isLoading = true;
                      });

                      try {

                        var prefs = await SharedPreferences.getInstance();
                        final userData = json.encode(
                          {
                            'userEmail': user.email,
                            'userUid': user.uid,
                          },
                        );
                        prefs.setString('userData', userData);

                        final imageUrl = await appUtils.uploadImageProfile(context, imageFile);

                        final _userDetails = UserDetails(
                          userEmail: emailController.text,
                          username: fullnameController.text,
                          userUid: user.uid,
                          userpic: imageUrl,
                          fastStartTime: startTime.toString(),
                          fastEndTime: endTime.toString(),
                          goals: selectGoals,
                          fastType: fastType,
                          active: false,
                          userWeight: "0",
                          fastingHour: "0",
                          fastingHoursLeft: "0",
                          fastStartedAt: DateTime.now().toString(),

                        );

                        await FirebaseFirestore.instance.doc("Users/${firebaseUtil.user.uid}").set(_userDetails.toJson());

                        // userDetails = await appUtils.getUserData();
                      } catch (e) {
                        print(e);
                      }
                      setState(() {
                        _isLoading = false;
                      });
                      AppRoutes.makeFirst(context, TabsScreen());
                    },
                    color: AppColors.darkBlue,
                    textColor: Colors.white,
                    child: Text('Start'),
                  ),
          ),

          showNextButton: true,
        ),
    );
  }
}
