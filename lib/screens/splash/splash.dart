import 'package:fasting_app/screens/tabs_view/tab_views.dart';
import 'package:fasting_app/util/firebase_service.dart';
import 'package:fasting_app/Auth/LoginScreen.dart';
import 'package:fasting_app/constants/images.dart';
import 'package:fasting_app/util/app_utils.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:get/get.dart';
import 'dart:async';



class SplashScreen extends StatefulWidget {

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  final FirebaseService firebaseUtil = GetIt.I.get<FirebaseService>();
  final AppUtils appUtils = GetIt.I.get<AppUtils>();


  @override
  void initState() {
    super.initState();
    Timer(Duration(seconds: 3), () async {
      if (firebaseUtil.user != null) {
        Get.offAll(() => TabsScreen());
      } else Get.offAll(() => Login());
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Image.asset(
            Images.splash,
            fit: BoxFit.cover,
          )),
    );
  }
}
