import 'package:fasting_app/util/preference_utils.dart';
import 'package:fasting_app/util/keys.dart';
import 'package:get/get.dart';


class AppLanguage extends GetxController {

  @override
  void onInit() {
    super.onInit();
  }

  void saveLanguage(String lang) async {
    await PreferenceUtils.setString(KEYS.LOCALE, lang);
    update();
  }
}