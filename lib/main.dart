import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:fasting_app/util/preference_utils.dart';
import 'package:fasting_app/screens/splash/splash.dart';
import 'package:fasting_app/util/app_utils_impl.dart';
import 'package:firebase_core/firebase_core.dart';
import 'injection/injection_container.dart' as di;
import 'package:fasting_app/util/keys.dart';
import 'package:provider/provider.dart';
import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'database/app_database.dart';
import 'langs/translation.dart';



Future<void> main() async {
  var db = AppDatabase.instance;
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await di.setup();
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => AppUtilsImpl()),
        Provider<AppDatabase>(
          create: (_) => db,
          dispose: (context, value) => value.close(),
        ),
      ],
      child: MyApp(),
    )
  );
}


class MyApp extends StatelessWidget {


  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    return GetMaterialApp(
      title: 'Fasting',
      debugShowCheckedModeBanner: false,
      translations: Translation(),
      locale: Locale(PreferenceUtils.getString(KEYS.LOCALE, 'en')),
      fallbackLocale: Locale('en'),
      theme: ThemeData(
        primarySwatch: Colors.purple,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: SplashScreen()
      // home: MyHomePage(title: 'Circular Countdown Timer'),
    );
  }
}