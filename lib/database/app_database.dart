import 'package:fasting_app/models/reminder_models.dart';
import 'package:moor_flutter/moor_flutter.dart';
import 'package:moor/moor.dart';

part 'app_database.g.dart';

@UseMoor(
    /// All Tables
    tables: [
      ReminderModels,
    ],
    /// All Daos
    daos: [
      ReminderModelsDao,
    ],
    /// All Queries
    queries: {

    },
)
class AppDatabase extends _$AppDatabase {

  AppDatabase() : super((FlutterQueryExecutor.inDatabaseFolder(
    path: 'db.fasting',
    logStatements: true,
  )));

  Future<void> deleteAllData() {
    return transaction(() async {
      for (var table in allTables) {
        await delete(table).go();
      }
    });
  }


  @override
  int get schemaVersion => 1;


  //! SINGLETON
  static final AppDatabase _singleton = new AppDatabase._internal();
  AppDatabase._internal() : super((FlutterQueryExecutor.inDatabaseFolder(
    path: 'db.fasting',
    logStatements: true,
  )));


  static AppDatabase get instance => _singleton;
}
