// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_database.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps, unnecessary_this
class ReminderModel extends DataClass implements Insertable<ReminderModel> {
  final int idReminder;
  final String title;
  final int timeReminder;
  final DateTime date;
  final DateTime dateInsert;
  ReminderModel(
      {@required this.idReminder,
      @required this.title,
      @required this.timeReminder,
      @required this.date,
      @required this.dateInsert});
  factory ReminderModel.fromData(
      Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return ReminderModel(
      idReminder: intType
          .mapFromDatabaseResponse(data['${effectivePrefix}id_reminder']),
      title:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}title']),
      timeReminder: intType
          .mapFromDatabaseResponse(data['${effectivePrefix}time_reminder']),
      date:
          dateTimeType.mapFromDatabaseResponse(data['${effectivePrefix}date']),
      dateInsert: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}date_insert']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || idReminder != null) {
      map['id_reminder'] = Variable<int>(idReminder);
    }
    if (!nullToAbsent || title != null) {
      map['title'] = Variable<String>(title);
    }
    if (!nullToAbsent || timeReminder != null) {
      map['time_reminder'] = Variable<int>(timeReminder);
    }
    if (!nullToAbsent || date != null) {
      map['date'] = Variable<DateTime>(date);
    }
    if (!nullToAbsent || dateInsert != null) {
      map['date_insert'] = Variable<DateTime>(dateInsert);
    }
    return map;
  }

  ReminderModelsCompanion toCompanion(bool nullToAbsent) {
    return ReminderModelsCompanion(
      idReminder: idReminder == null && nullToAbsent
          ? const Value.absent()
          : Value(idReminder),
      title:
          title == null && nullToAbsent ? const Value.absent() : Value(title),
      timeReminder: timeReminder == null && nullToAbsent
          ? const Value.absent()
          : Value(timeReminder),
      date: date == null && nullToAbsent ? const Value.absent() : Value(date),
      dateInsert: dateInsert == null && nullToAbsent
          ? const Value.absent()
          : Value(dateInsert),
    );
  }

  factory ReminderModel.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return ReminderModel(
      idReminder: serializer.fromJson<int>(json['idReminder']),
      title: serializer.fromJson<String>(json['title']),
      timeReminder: serializer.fromJson<int>(json['timeReminder']),
      date: serializer.fromJson<DateTime>(json['date']),
      dateInsert: serializer.fromJson<DateTime>(json['dateInsert']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'idReminder': serializer.toJson<int>(idReminder),
      'title': serializer.toJson<String>(title),
      'timeReminder': serializer.toJson<int>(timeReminder),
      'date': serializer.toJson<DateTime>(date),
      'dateInsert': serializer.toJson<DateTime>(dateInsert),
    };
  }

  ReminderModel copyWith(
          {int idReminder,
          String title,
          int timeReminder,
          DateTime date,
          DateTime dateInsert}) =>
      ReminderModel(
        idReminder: idReminder ?? this.idReminder,
        title: title ?? this.title,
        timeReminder: timeReminder ?? this.timeReminder,
        date: date ?? this.date,
        dateInsert: dateInsert ?? this.dateInsert,
      );
  @override
  String toString() {
    return (StringBuffer('ReminderModel(')
          ..write('idReminder: $idReminder, ')
          ..write('title: $title, ')
          ..write('timeReminder: $timeReminder, ')
          ..write('date: $date, ')
          ..write('dateInsert: $dateInsert')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      idReminder.hashCode,
      $mrjc(
          title.hashCode,
          $mrjc(timeReminder.hashCode,
              $mrjc(date.hashCode, dateInsert.hashCode)))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is ReminderModel &&
          other.idReminder == this.idReminder &&
          other.title == this.title &&
          other.timeReminder == this.timeReminder &&
          other.date == this.date &&
          other.dateInsert == this.dateInsert);
}

class ReminderModelsCompanion extends UpdateCompanion<ReminderModel> {
  final Value<int> idReminder;
  final Value<String> title;
  final Value<int> timeReminder;
  final Value<DateTime> date;
  final Value<DateTime> dateInsert;
  const ReminderModelsCompanion({
    this.idReminder = const Value.absent(),
    this.title = const Value.absent(),
    this.timeReminder = const Value.absent(),
    this.date = const Value.absent(),
    this.dateInsert = const Value.absent(),
  });
  ReminderModelsCompanion.insert({
    this.idReminder = const Value.absent(),
    @required String title,
    @required int timeReminder,
    @required DateTime date,
    @required DateTime dateInsert,
  })  : title = Value(title),
        timeReminder = Value(timeReminder),
        date = Value(date),
        dateInsert = Value(dateInsert);
  static Insertable<ReminderModel> custom({
    Expression<int> idReminder,
    Expression<String> title,
    Expression<int> timeReminder,
    Expression<DateTime> date,
    Expression<DateTime> dateInsert,
  }) {
    return RawValuesInsertable({
      if (idReminder != null) 'id_reminder': idReminder,
      if (title != null) 'title': title,
      if (timeReminder != null) 'time_reminder': timeReminder,
      if (date != null) 'date': date,
      if (dateInsert != null) 'date_insert': dateInsert,
    });
  }

  ReminderModelsCompanion copyWith(
      {Value<int> idReminder,
      Value<String> title,
      Value<int> timeReminder,
      Value<DateTime> date,
      Value<DateTime> dateInsert}) {
    return ReminderModelsCompanion(
      idReminder: idReminder ?? this.idReminder,
      title: title ?? this.title,
      timeReminder: timeReminder ?? this.timeReminder,
      date: date ?? this.date,
      dateInsert: dateInsert ?? this.dateInsert,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (idReminder.present) {
      map['id_reminder'] = Variable<int>(idReminder.value);
    }
    if (title.present) {
      map['title'] = Variable<String>(title.value);
    }
    if (timeReminder.present) {
      map['time_reminder'] = Variable<int>(timeReminder.value);
    }
    if (date.present) {
      map['date'] = Variable<DateTime>(date.value);
    }
    if (dateInsert.present) {
      map['date_insert'] = Variable<DateTime>(dateInsert.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('ReminderModelsCompanion(')
          ..write('idReminder: $idReminder, ')
          ..write('title: $title, ')
          ..write('timeReminder: $timeReminder, ')
          ..write('date: $date, ')
          ..write('dateInsert: $dateInsert')
          ..write(')'))
        .toString();
  }
}

class $ReminderModelsTable extends ReminderModels
    with TableInfo<$ReminderModelsTable, ReminderModel> {
  final GeneratedDatabase _db;
  final String _alias;
  $ReminderModelsTable(this._db, [this._alias]);
  final VerificationMeta _idReminderMeta = const VerificationMeta('idReminder');
  GeneratedIntColumn _idReminder;
  @override
  GeneratedIntColumn get idReminder => _idReminder ??= _constructIdReminder();
  GeneratedIntColumn _constructIdReminder() {
    return GeneratedIntColumn(
      'id_reminder',
      $tableName,
      false,
    );
  }

  final VerificationMeta _titleMeta = const VerificationMeta('title');
  GeneratedTextColumn _title;
  @override
  GeneratedTextColumn get title => _title ??= _constructTitle();
  GeneratedTextColumn _constructTitle() {
    return GeneratedTextColumn(
      'title',
      $tableName,
      false,
    );
  }

  final VerificationMeta _timeReminderMeta =
      const VerificationMeta('timeReminder');
  GeneratedIntColumn _timeReminder;
  @override
  GeneratedIntColumn get timeReminder =>
      _timeReminder ??= _constructTimeReminder();
  GeneratedIntColumn _constructTimeReminder() {
    return GeneratedIntColumn(
      'time_reminder',
      $tableName,
      false,
    );
  }

  final VerificationMeta _dateMeta = const VerificationMeta('date');
  GeneratedDateTimeColumn _date;
  @override
  GeneratedDateTimeColumn get date => _date ??= _constructDate();
  GeneratedDateTimeColumn _constructDate() {
    return GeneratedDateTimeColumn(
      'date',
      $tableName,
      false,
    );
  }

  final VerificationMeta _dateInsertMeta = const VerificationMeta('dateInsert');
  GeneratedDateTimeColumn _dateInsert;
  @override
  GeneratedDateTimeColumn get dateInsert =>
      _dateInsert ??= _constructDateInsert();
  GeneratedDateTimeColumn _constructDateInsert() {
    return GeneratedDateTimeColumn(
      'date_insert',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns =>
      [idReminder, title, timeReminder, date, dateInsert];
  @override
  $ReminderModelsTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'reminder_models';
  @override
  final String actualTableName = 'reminder_models';
  @override
  VerificationContext validateIntegrity(Insertable<ReminderModel> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id_reminder')) {
      context.handle(
          _idReminderMeta,
          idReminder.isAcceptableOrUnknown(
              data['id_reminder'], _idReminderMeta));
    }
    if (data.containsKey('title')) {
      context.handle(
          _titleMeta, title.isAcceptableOrUnknown(data['title'], _titleMeta));
    } else if (isInserting) {
      context.missing(_titleMeta);
    }
    if (data.containsKey('time_reminder')) {
      context.handle(
          _timeReminderMeta,
          timeReminder.isAcceptableOrUnknown(
              data['time_reminder'], _timeReminderMeta));
    } else if (isInserting) {
      context.missing(_timeReminderMeta);
    }
    if (data.containsKey('date')) {
      context.handle(
          _dateMeta, date.isAcceptableOrUnknown(data['date'], _dateMeta));
    } else if (isInserting) {
      context.missing(_dateMeta);
    }
    if (data.containsKey('date_insert')) {
      context.handle(
          _dateInsertMeta,
          dateInsert.isAcceptableOrUnknown(
              data['date_insert'], _dateInsertMeta));
    } else if (isInserting) {
      context.missing(_dateInsertMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {idReminder};
  @override
  ReminderModel map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return ReminderModel.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $ReminderModelsTable createAlias(String alias) {
    return $ReminderModelsTable(_db, alias);
  }
}

abstract class _$AppDatabase extends GeneratedDatabase {
  _$AppDatabase(QueryExecutor e) : super(SqlTypeSystem.defaultInstance, e);
  $ReminderModelsTable _reminderModels;
  $ReminderModelsTable get reminderModels =>
      _reminderModels ??= $ReminderModelsTable(this);
  ReminderModelsDao _reminderModelsDao;
  ReminderModelsDao get reminderModelsDao =>
      _reminderModelsDao ??= ReminderModelsDao(this as AppDatabase);
  @override
  Iterable<TableInfo> get allTables => allSchemaEntities.whereType<TableInfo>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities => [reminderModels];
}
