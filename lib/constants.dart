import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'constants/images.dart';



final feelingOptions = {
  3: Images.laugh,
  2: Images.appy,
  1: Images.mean,
  0: Images.sad,
  -1: Images.cry,
  -2: Images.dead,
};