// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'reminder_models.dart';

// **************************************************************************
// DaoGenerator
// **************************************************************************

mixin _$ReminderModelsDaoMixin on DatabaseAccessor<AppDatabase> {
  $ReminderModelsTable get reminderModels => attachedDatabase.reminderModels;
}
