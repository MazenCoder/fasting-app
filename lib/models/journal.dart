import 'package:cloud_firestore/cloud_firestore.dart';


class Journal {

  final String id;
  final String uid;
  final Timestamp timestamp;
  final String note;
  final String weight;
  final int idFeeling;
  final String staredFasting;
  final String goalFasting;

  Journal({
    this.id, this.uid, this.timestamp, this.note,
    this.weight, this.idFeeling, this.staredFasting,
    this.goalFasting,
  });

  toJson() {
    return {
      "id": id,
      "uid": uid,
      "note": note,
      "weight": weight,
      "idFeeling": idFeeling,
      "staredFasting": staredFasting,
      "goalFasting": goalFasting,
      "timestamp": FieldValue.serverTimestamp(),
    };
  }

  factory Journal.fromJson(Map<String, dynamic> json) => Journal(
    id: json["id"],
    uid: json["uid"],
    note: json["note"],
    weight: json["weight"],
    idFeeling: json["idFeeling"],
    goalFasting: json["goalFasting"],
    staredFasting: json["staredFasting"],
    timestamp: json["timestamp"],
  );
}