import 'package:fasting_app/database/app_database.dart';
import 'package:moor/moor.dart';

part 'reminder_models.g.dart';

class ReminderModels extends Table {


  IntColumn get idReminder => integer()();
  TextColumn get title => text()();
  IntColumn get timeReminder => integer()();
  DateTimeColumn get date => dateTime()();
  DateTimeColumn get dateInsert => dateTime()();

  @override
  Set<Column> get primaryKey => {idReminder};

}

@UseDao(tables: [ReminderModels])
class ReminderModelsDao extends DatabaseAccessor<AppDatabase>
    with _$ReminderModelsDaoMixin {

  final AppDatabase db;
  ReminderModelsDao(this.db) : super(db);

  Stream<List<ReminderModel>> watchAllReminder() => select(reminderModels).watch();
  Future<List<ReminderModel>> getAllReminder() => select(reminderModels).get();

  Future<void> insertAllReminder(List<Insertable<ReminderModel>> rows) => batch((batch) => batch.insertAll(reminderModels, rows, mode: InsertMode.replace));
  Future insertReminder(Insertable<ReminderModel> row) => into(reminderModels).insert(row, mode: InsertMode.replace);
  Future updateReminder(Insertable<ReminderModel> row) => update(reminderModels).replace(row);
  Future deleteReminder(Insertable<ReminderModel> row) => delete(reminderModels).delete(row);


  Stream<List<ReminderModel>> watchReminderByDate() {
  return (select(reminderModels)
    ..orderBy([
        (t) => OrderingTerm(expression: t.dateInsert, mode: OrderingMode.desc),
    ])).watch();
  }
}
