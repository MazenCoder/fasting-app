import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fasting_app/database/app_database.dart';
import 'package:fasting_app/util/firebase_service.dart';
import 'package:fasting_app/util/preference_utils.dart';
import 'package:fasting_app/network/network_info.dart';
import 'package:fasting_app/util/app_utils_impl.dart';
import 'package:fasting_app/util/app_utils.dart';
import 'package:http/http.dart' as http;
import 'package:get_it/get_it.dart';



final GetIt sl = GetIt.instance;


Future<void> setup() async {
  try {
    await init();
    await initUtilsImpl();
  } catch(e) {
    print('error, setup: $e');
  }
}

///!  init
Future<void> init() async {
  //! Firebase
  try {

    //! Database
    final db = AppDatabase.instance;
    sl.registerLazySingleton(() => db);

    //! Network
    sl.registerLazySingleton<NetworkInfo>(() => NetworkInfoImpl(sl()));

    //! External
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sl.registerLazySingleton(() => DataConnectionChecker());
    sl.registerLazySingleton(() => sharedPreferences);
    sl.registerLazySingleton(() => http.Client());

   //! Preference
   PreferenceUtils instance = await PreferenceUtils.init();
   sl.registerSingleton<PreferenceUtils>(instance);

    //! Firebase
    sl.registerLazySingleton<FirebaseService>(() => FirebaseService());

  } catch(e) {
    print('error, init: $e');
  }
}

///!  initUtilsImpl
Future<void> initUtilsImpl() async {
  sl.registerSingleton<AppUtils>(AppUtilsImpl(preferences: sl(),
      client: sl(), networkInfo: sl(), database: sl()), signalsReady: true);
}