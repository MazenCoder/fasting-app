import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:fasting_app/injection/injection_container.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fasting_app/database/app_database.dart';
import 'package:fasting_app/util/firebase_service.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fasting_app/network/network_info.dart';
import 'package:fasting_app/util/flash_helper.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:path_provider/path_provider.dart';
import 'package:fasting_app/models/journal.dart';
import 'package:image_picker/image_picker.dart';
import 'package:fasting_app/models/models.dart';
import 'package:fasting_app/util/keys.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart' as p;
import 'package:logger/logger.dart';
import 'package:get_it/get_it.dart';
import 'dart:ui' as ui show Codec;
import 'package:path/path.dart';
import 'package:uuid/uuid.dart';
import 'package:get/get.dart';
import 'dart:typed_data';
import 'app_utils.dart';
import 'dart:isolate';
import 'dart:convert';
import 'dart:math';
import 'dart:io';
import 'dart:ui';



class AppUtilsImpl extends AppUtils {


  final firebase_storage.FirebaseStorage storage = firebase_storage.FirebaseStorage.instance;
  final FirebaseService firebaseUtil = GetIt.I.get<FirebaseService>();
  final NetworkInfo networkInfo;
  SharedPreferences preferences;
  final picker = ImagePicker();
  final AppDatabase database;
  final http.Client client;
  var logger = Logger();



  AppUtilsImpl({this.preferences, this.client, this.networkInfo, this.database}) {
    Future.delayed(Duration(seconds: 3)).then((_) => sl.signalReady(this));
  }


  @override
  Future<UserDetails> getUserData() async {
    if (firebaseUtil.user != null) {
      DocumentSnapshot doc = await firebaseUtil.cloud.doc('Users/${firebaseUtil.user.uid}').get();
      if (doc != null && doc.exists) {
        return UserDetails.fromJson(doc.data());
      }
    }
  }


  Future uploadFiles({BuildContext context, List<File> files, String category}) async {
    try {
      if (await networkInfo.isConnected) {
        var rng = new Random();

        firebase_storage.SettableMetadata metadata;

        List<String> _downloadUrl = [];
        List<firebase_storage.UploadTask> _uploadTasks = [];
        DocumentReference reference = firebaseUtil.cloud.doc(KEYS.STORAGE+'/'+category);


        for (File file in files) {
          String name = basename(file.path);
          // final _rng = rng.nextInt(9999);
          var uuid = Uuid();
          firebase_storage.Reference ref = firebase_storage.FirebaseStorage.instance.ref("$category/${uuid.v4()}$name");

          //! Videos
          if (file.path.contains('.mp4') || file.path.contains('.MP4')) {
            metadata = firebase_storage.SettableMetadata(
              contentType: 'video/mp4', customMetadata: {'picked-file-path': file.path},
            );

            await ref.putFile(file, metadata).then((firebase_storage.TaskSnapshot snapshot) async {
              final url = await snapshot.ref.getDownloadURL();
              _downloadUrl.add(url);
            });

            //! Images
          } else if (file.path.contains('.png') || file.path.contains('.PNG')) {
            metadata = firebase_storage.SettableMetadata(
              contentType: 'image/png', customMetadata: {'picked-file-path': file.path},
            );

            await ref.putFile(file, metadata).then((firebase_storage.TaskSnapshot snapshot) async {
              final url = await snapshot.ref.getDownloadURL();
              _downloadUrl.add(url);
            });
          } else if (file.path.contains('.jpg') || file.path.contains('.JPG')) {
            metadata = firebase_storage.SettableMetadata(
              contentType: 'image/jpg', customMetadata: {'picked-file-path': file.path},
            );

            await ref.putFile(file, metadata).then((firebase_storage.TaskSnapshot snapshot) async {
              final url = await snapshot.ref.getDownloadURL();
              _downloadUrl.add(url);
            });
          } else if (file.path.contains('.jpeg') || file.path.contains('.JPEG')) {
            metadata = firebase_storage.SettableMetadata(
              contentType: 'image/jpeg', customMetadata: {'picked-file-path': file.path},
            );

            await ref.putFile(file, metadata).then((firebase_storage.TaskSnapshot snapshot) async {
              final url = await snapshot.ref.getDownloadURL();
              _downloadUrl.add(url);
            });
          }  else {
            return FlashHelper.errorBar(context, message: 'convert_file'.tr);
          }
        }

        final documentSnapshot = await reference.get();
        if (documentSnapshot.data() != null) {
          print('update....');
          // final model = categoriesModelFromJson(documentSnapshot.data());
          // _downloadUrl.addAll(model.urls);
          await reference.update({
            // 'category': category,
            'urls': _downloadUrl,
          }).whenComplete(() {
            FlashHelper.successBar(context, message: 'completed_successfully'.tr);
          }).catchError((e) {
            FlashHelper.errorBar(context, message: 'something_wrong'.tr);
          });
        } else {
          print('set....');
          await reference.set({
            'category': category,
            'urls': _downloadUrl,
          }).whenComplete(() {
            FlashHelper.successBar(context, message: 'completed_successfully'.tr);
          }).catchError((e) {
            FlashHelper.errorBar(context, message: 'something_wrong'.tr);
          });
        }

      } else {
        FlashHelper.infoBar(context, message: 'error_connection'.tr);
      }
      return;
    } catch (e) {
      return;
    }
  }

  @override
  Future<String> uploadImageProfile(BuildContext context, File file) async {
    try {
      if (await networkInfo.isConnected) {

        firebase_storage.SettableMetadata metadata;
        String _downloadUrl = '';

        String name = basename(file.path);
        var uuid = Uuid();
        firebase_storage.Reference ref = firebase_storage.FirebaseStorage.instance.ref("Users/${firebaseUtil.user.uid}/${uuid.v4()}$name");

        if (file.path.contains('.png') || file.path.contains('.PNG')) {
          metadata = firebase_storage.SettableMetadata(
            contentType: 'image/png', customMetadata: {'picked-file-path': file.path},
          );

          await ref.putFile(file, metadata).then((firebase_storage.TaskSnapshot snapshot) async {
            final url = await snapshot.ref.getDownloadURL();
            _downloadUrl = url;
          });
        } else if (file.path.contains('.jpg') || file.path.contains('.JPG')) {
          metadata = firebase_storage.SettableMetadata(
            contentType: 'image/jpg', customMetadata: {'picked-file-path': file.path},
          );

          await ref.putFile(file, metadata).then((firebase_storage.TaskSnapshot snapshot) async {
            final url = await snapshot.ref.getDownloadURL();
            _downloadUrl = url;
          });
        } else if (file.path.contains('.jpeg') || file.path.contains('.JPEG')) {
          metadata = firebase_storage.SettableMetadata(
            contentType: 'image/jpeg', customMetadata: {'picked-file-path': file.path},
          );

          await ref.putFile(file, metadata).then((firebase_storage.TaskSnapshot snapshot) async {
            final url = await snapshot.ref.getDownloadURL();
            _downloadUrl = url;
          });
        }  else {
          return FlashHelper.errorBar(context, message: 'convert_file'.tr);
        }

        return _downloadUrl;

      } else {
        FlashHelper.infoBar(context, message: 'error_connection'.tr);
      }
    } catch(e) {
      return FlashHelper.errorBar(context, message: 'convert_file'.tr);
    }
  }

  @override
  Future<void> updateImageProfile(BuildContext context, File file, String oldPic) async {
    try {
      if (await networkInfo.isConnected) {
        await _deleteImage(oldPic);
        final url = await uploadImageProfile(context, file);
        return await firebaseUtil.cloud.doc('Users/${firebaseUtil.user.uid}').update({"userpic": url ?? ''});
      } else {
        FlashHelper.infoBar(context, message: 'error_connection'.tr);
      }
    } catch(e) {
      logger.e('error: $e');
    }
  }

  _deleteImage(String url) async {
    try {
      if (url != null) {
        return await firebaseUtil.storage.refFromURL(url.trim()).delete();
      }
    } catch(e) {}
  }

  String _formatDuration(Duration d) {
    String f(int n) {
      return n.toString().padLeft(2, '0');
    }
    // We want to round up the remaining time to the nearest second
    d += Duration(microseconds: 999999);
    return "${f(d.inMinutes)}:${f(d.inSeconds%60)}";
  }

  String formatDuration(Duration duration) {
    String twoDigits(int n) => n.toString().padLeft(2, "0");
    String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
    String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));
    return "${twoDigits(duration.inHours)}:$twoDigitMinutes:$twoDigitSeconds";
  }

  String endFormatDuration(Duration duration) {
    String twoDigits(int n) => n.toString().padLeft(2, "0");
    String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
    return "${twoDigits(duration.inHours)} hour and $twoDigitMinutes minutes ";
  }

  @override
  Future<void> addJournal(Journal journal, String idDoc) async {
    // await firebaseUtil.cloud.doc('Users/${firebaseUtil.user.uid}').update({"active": false});
    await firebaseUtil.cloud.collection('Journal').doc(idDoc).set(journal.toJson());
  }

  @override
  Future<List<double>> getSparklineData() async {
    List<double> datas = [];
    QuerySnapshot query = await firebaseUtil.cloud.collection('Journal')
        .where('uid', isEqualTo: firebaseUtil.user.uid)
        .orderBy('timestamp', descending: true).get();

    final docs = query?.docs ?? [];

    print('docs: $docs');

    for (QueryDocumentSnapshot doc in docs) {
      print('docs: ${doc.data()}');

      final journal = Journal.fromJson(doc.data());
      print('docs: ${journal.toJson()}');
      if (journal.idFeeling != null) {
        datas.insert(0, journal.idFeeling.toDouble());
      }
    }
    return datas;
  }

  @override
  Future<void> saveReminder(ReminderModel model) async {
    await database.reminderModelsDao.insertReminder(model);
  }
}