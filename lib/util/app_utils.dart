import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fasting_app/database/app_database.dart';
import 'package:fasting_app/models/journal.dart';
import 'package:fasting_app/models/models.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'dart:io';


abstract class AppUtils extends ChangeNotifier {

  Future<UserDetails> getUserData();
  Future<String> uploadImageProfile(BuildContext context, File imageFile);
  Future<void> updateImageProfile(BuildContext context, File file, String oldPic);
  String formatDuration(Duration duration);
  String endFormatDuration(Duration duration);
  Future<void> addJournal(Journal journal, String idDoc);
  Future<List<double>> getSparklineData();
  Future<void> saveReminder(ReminderModel model);

}
