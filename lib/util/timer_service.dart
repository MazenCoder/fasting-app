import 'package:flutter/material.dart';
import 'dart:async';


Timer timer;
Stopwatch watch;
class TimerService extends ChangeNotifier {

  Duration get currentDuration => getCurrentDuration;
  Duration getCurrentDuration = Duration.zero;

  bool get isRunning => timer != null;

  TimerService() {
    watch = Stopwatch();
  }

  void onTick(Timer timer) {
    getCurrentDuration = watch?.elapsed;
    notifyListeners();
  }

  void start() async {
    try {
      if (timer != null) return;

      timer = Timer.periodic(Duration(seconds: 1), onTick);
      watch.start();

      notifyListeners();
      print('.. start ..');
    } catch(e) {}
  }

  void stop() async {
    try {
      timer?.cancel();
      timer = null;
      watch.stop();
      getCurrentDuration = watch.elapsed;

      // notifyListeners();
      
    } catch(e){}
  }

  void reset() {
    stop();
    watch?.reset();
    getCurrentDuration = Duration.zero;
    notifyListeners();
  }

  static TimerService of(BuildContext context) {
    var provider = context.dependOnInheritedWidgetOfExactType<TimerServiceProvider>();
    return provider.service;
  }
}

class TimerServiceProvider extends InheritedWidget {
  const TimerServiceProvider({Key key, this.service, Widget child}) : super(key: key, child: child);

  final TimerService service;

  @override
  bool updateShouldNotify(TimerServiceProvider old) => service != old.service;
}