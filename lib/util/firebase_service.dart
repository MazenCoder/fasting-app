import 'package:fasting_app/util/preference_utils.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';


class FirebaseService {

  final FirebaseAuth auth = FirebaseAuth.instance;
  final FirebaseFirestore cloud = FirebaseFirestore.instance;
  final FirebaseStorage storage = FirebaseStorage.instance;

  User get user => auth.currentUser;

  // --- sign in, up and out
  Future<String> signIn(String email, String password) async {
    UserCredential result = await auth.signInWithEmailAndPassword(email: email, password: password);
    User user = result.user;
    return user.uid;
  }

  Future<String> signUp(String email, String password) async {
    UserCredential result = await auth.createUserWithEmailAndPassword(email: email, password: password);
    User user = result.user;
    return user.uid;
  }

  Future<void> signOut() async {
    await PreferenceUtils.removeKey('userData');
    return await auth?.signOut();
  }
}