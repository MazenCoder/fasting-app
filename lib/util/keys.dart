class KEYS {

  static const String LOCALE = "Locale";
  static const String INTRO = "INTRO";
  static const String FCM_TOKENS = "FCM_TOKENS";

  static const String POSTS = "posts";
  static const String USERS_POSTS = "users_posts";

  static const String LIKES = "likes";
  static const String POST_LIKES = "post_likes";

  static const String FOLLOWERS = "followers";
  static const String FOLLOWING = "following";

  static const String USER_FOLLOWERS = "user_followers";
  static const String USER_FOLLOWING = "user_following";


  static const String NOTIFICATION = "notification";
  static const String CATEGORIES = "categories";


  static const String USERS = "users";
  static const String PHOTO_USERS_PROFILE = "photo_users_profile";
  static const String STORAGE = "storage";



  static const String COMMENT = "comments";
  static const String POST_COMMENT = "post_comments";



  /// HEADER SERVICE

}